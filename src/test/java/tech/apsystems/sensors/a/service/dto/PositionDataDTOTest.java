package tech.apsystems.sensors.a.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import tech.apsystems.sensors.a.web.rest.TestUtil;

public class PositionDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PositionDataDTO.class);
        PositionDataDTO positionDataDTO1 = new PositionDataDTO();
        positionDataDTO1.setId("id1");
        PositionDataDTO positionDataDTO2 = new PositionDataDTO();
        assertThat(positionDataDTO1).isNotEqualTo(positionDataDTO2);
        positionDataDTO2.setId(positionDataDTO1.getId());
        assertThat(positionDataDTO1).isEqualTo(positionDataDTO2);
        positionDataDTO2.setId("id2");
        assertThat(positionDataDTO1).isNotEqualTo(positionDataDTO2);
        positionDataDTO1.setId(null);
        assertThat(positionDataDTO1).isNotEqualTo(positionDataDTO2);
    }
}
