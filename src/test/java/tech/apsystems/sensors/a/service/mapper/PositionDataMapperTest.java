package tech.apsystems.sensors.a.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PositionDataMapperTest {

    private PositionDataMapper positionDataMapper;

    @BeforeEach
    public void setUp() {
        positionDataMapper = new PositionDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        String id = "id1";
        assertThat(positionDataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(positionDataMapper.fromId(null)).isNull();
    }
}
