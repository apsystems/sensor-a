package tech.apsystems.sensors.a.web.rest;

import tech.apsystems.sensors.a.SensoraApp;
import tech.apsystems.sensors.a.config.TestSecurityConfiguration;
import tech.apsystems.sensors.a.domain.PositionData;
import tech.apsystems.sensors.a.repository.PositionDataRepository;
import tech.apsystems.sensors.a.service.PositionDataService;
import tech.apsystems.sensors.a.service.dto.PositionDataDTO;
import tech.apsystems.sensors.a.service.mapper.PositionDataMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

/**
 * Integration tests for the {@link PositionDataResource} REST controller.
 */
@SpringBootTest(classes = { SensoraApp.class, TestSecurityConfiguration.class })
@AutoConfigureWebTestClient
@WithMockUser
public class PositionDataResourceIT {

    private static final Double DEFAULT_LAT = 1D;
    private static final Double UPDATED_LAT = 2D;

    private static final Double DEFAULT_LON = 1D;
    private static final Double UPDATED_LON = 2D;

    private static final Double DEFAULT_ALTITUDE = 1D;
    private static final Double UPDATED_ALTITUDE = 2D;

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private PositionDataRepository positionDataRepository;

    @Autowired
    private PositionDataMapper positionDataMapper;

    @Autowired
    private PositionDataService positionDataService;

    @Autowired
    private WebTestClient webTestClient;

    private PositionData positionData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PositionData createEntity() {
        PositionData positionData = new PositionData()
            .lat(DEFAULT_LAT)
            .lon(DEFAULT_LON)
            .altitude(DEFAULT_ALTITUDE)
            .date(DEFAULT_DATE);
        return positionData;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PositionData createUpdatedEntity() {
        PositionData positionData = new PositionData()
            .lat(UPDATED_LAT)
            .lon(UPDATED_LON)
            .altitude(UPDATED_ALTITUDE)
            .date(UPDATED_DATE);
        return positionData;
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        positionDataRepository.deleteAll().block();
        positionData = createEntity();
    }

    @Test
    public void createPositionData() throws Exception {
        int databaseSizeBeforeCreate = positionDataRepository.findAll().collectList().block().size();
        // Create the PositionData
        PositionDataDTO positionDataDTO = positionDataMapper.toDto(positionData);
        webTestClient.post().uri("/api/position-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(positionDataDTO))
            .exchange()
            .expectStatus().isCreated();

        // Validate the PositionData in the database
        List<PositionData> positionDataList = positionDataRepository.findAll().collectList().block();
        assertThat(positionDataList).hasSize(databaseSizeBeforeCreate + 1);
        PositionData testPositionData = positionDataList.get(positionDataList.size() - 1);
        assertThat(testPositionData.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testPositionData.getLon()).isEqualTo(DEFAULT_LON);
        assertThat(testPositionData.getAltitude()).isEqualTo(DEFAULT_ALTITUDE);
        assertThat(testPositionData.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    public void createPositionDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = positionDataRepository.findAll().collectList().block().size();

        // Create the PositionData with an existing ID
        positionData.setId("existing_id");
        PositionDataDTO positionDataDTO = positionDataMapper.toDto(positionData);

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient.post().uri("/api/position-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(positionDataDTO))
            .exchange()
            .expectStatus().isBadRequest();

        // Validate the PositionData in the database
        List<PositionData> positionDataList = positionDataRepository.findAll().collectList().block();
        assertThat(positionDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllPositionData() {
        // Initialize the database
        positionDataRepository.save(positionData).block();

        // Get all the positionDataList
        webTestClient.get().uri("/api/position-data?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id").value(hasItem(positionData.getId()))
            .jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.doubleValue()))
            .jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.doubleValue()))
            .jsonPath("$.[*].altitude").value(hasItem(DEFAULT_ALTITUDE.doubleValue()))
            .jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString()));
    }
    
    @Test
    public void getPositionData() {
        // Initialize the database
        positionDataRepository.save(positionData).block();

        // Get the positionData
        webTestClient.get().uri("/api/position-data/{id}", positionData.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id").value(is(positionData.getId()))
            .jsonPath("$.lat").value(is(DEFAULT_LAT.doubleValue()))
            .jsonPath("$.lon").value(is(DEFAULT_LON.doubleValue()))
            .jsonPath("$.altitude").value(is(DEFAULT_ALTITUDE.doubleValue()))
            .jsonPath("$.date").value(is(DEFAULT_DATE.toString()));
    }
    @Test
    public void getNonExistingPositionData() {
        // Get the positionData
        webTestClient.get().uri("/api/position-data/{id}", Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound();
    }

    @Test
    public void updatePositionData() throws Exception {
        // Initialize the database
        positionDataRepository.save(positionData).block();

        int databaseSizeBeforeUpdate = positionDataRepository.findAll().collectList().block().size();

        // Update the positionData
        PositionData updatedPositionData = positionDataRepository.findById(positionData.getId()).block();
        updatedPositionData
            .lat(UPDATED_LAT)
            .lon(UPDATED_LON)
            .altitude(UPDATED_ALTITUDE)
            .date(UPDATED_DATE);
        PositionDataDTO positionDataDTO = positionDataMapper.toDto(updatedPositionData);

        webTestClient.put().uri("/api/position-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(positionDataDTO))
            .exchange()
            .expectStatus().isOk();

        // Validate the PositionData in the database
        List<PositionData> positionDataList = positionDataRepository.findAll().collectList().block();
        assertThat(positionDataList).hasSize(databaseSizeBeforeUpdate);
        PositionData testPositionData = positionDataList.get(positionDataList.size() - 1);
        assertThat(testPositionData.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testPositionData.getLon()).isEqualTo(UPDATED_LON);
        assertThat(testPositionData.getAltitude()).isEqualTo(UPDATED_ALTITUDE);
        assertThat(testPositionData.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    public void updateNonExistingPositionData() throws Exception {
        int databaseSizeBeforeUpdate = positionDataRepository.findAll().collectList().block().size();

        // Create the PositionData
        PositionDataDTO positionDataDTO = positionDataMapper.toDto(positionData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient.put().uri("/api/position-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(positionDataDTO))
            .exchange()
            .expectStatus().isBadRequest();

        // Validate the PositionData in the database
        List<PositionData> positionDataList = positionDataRepository.findAll().collectList().block();
        assertThat(positionDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deletePositionData() {
        // Initialize the database
        positionDataRepository.save(positionData).block();

        int databaseSizeBeforeDelete = positionDataRepository.findAll().collectList().block().size();

        // Delete the positionData
        webTestClient.delete().uri("/api/position-data/{id}", positionData.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNoContent();

        // Validate the database contains one less item
        List<PositionData> positionDataList = positionDataRepository.findAll().collectList().block();
        assertThat(positionDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
