package tech.apsystems.sensors.a.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import tech.apsystems.sensors.a.web.rest.TestUtil;

public class PositionDataTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PositionData.class);
        PositionData positionData1 = new PositionData();
        positionData1.setId("id1");
        PositionData positionData2 = new PositionData();
        positionData2.setId(positionData1.getId());
        assertThat(positionData1).isEqualTo(positionData2);
        positionData2.setId("id2");
        assertThat(positionData1).isNotEqualTo(positionData2);
        positionData1.setId(null);
        assertThat(positionData1).isNotEqualTo(positionData2);
    }
}
