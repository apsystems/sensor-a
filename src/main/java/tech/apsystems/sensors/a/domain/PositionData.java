package tech.apsystems.sensors.a.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;

/**
 * A PositionData.
 */
@Document(collection = "position_data")
public class PositionData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("lat")
    private Double lat;

    @Field("lon")
    private Double lon;

    @Field("altitude")
    private Double altitude;

    @Field("date")
    private Instant date;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public PositionData lat(Double lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public PositionData lon(Double lon) {
        this.lon = lon;
        return this;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getAltitude() {
        return altitude;
    }

    public PositionData altitude(Double altitude) {
        this.altitude = altitude;
        return this;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Instant getDate() {
        return date;
    }

    public PositionData date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PositionData)) {
            return false;
        }
        return id != null && id.equals(((PositionData) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PositionData{" +
            "id=" + getId() +
            ", lat=" + getLat() +
            ", lon=" + getLon() +
            ", altitude=" + getAltitude() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
