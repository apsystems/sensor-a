/**
 * View Models used by Spring MVC REST controllers.
 */
package tech.apsystems.sensors.a.web.rest.vm;
