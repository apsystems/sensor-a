package tech.apsystems.sensors.a.web.rest;

import tech.apsystems.sensors.a.service.PositionDataService;
import tech.apsystems.sensors.a.web.rest.errors.BadRequestAlertException;
import tech.apsystems.sensors.a.service.dto.PositionDataDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.reactive.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link tech.apsystems.sensors.a.domain.PositionData}.
 */
@RestController
@RequestMapping("/api")
public class PositionDataResource {

    private final Logger log = LoggerFactory.getLogger(PositionDataResource.class);

    private static final String ENTITY_NAME = "sensoraPositionData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PositionDataService positionDataService;

    public PositionDataResource(PositionDataService positionDataService) {
        this.positionDataService = positionDataService;
    }

    /**
     * {@code POST  /position-data} : Create a new positionData.
     *
     * @param positionDataDTO the positionDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new positionDataDTO, or with status {@code 400 (Bad Request)} if the positionData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/position-data")
    public Mono<ResponseEntity<PositionDataDTO>> createPositionData(@RequestBody PositionDataDTO positionDataDTO) throws URISyntaxException {
        log.debug("REST request to save PositionData : {}", positionDataDTO);
        if (positionDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new positionData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return positionDataService.save(positionDataDTO)
            .map(result -> {
                try {
                    return ResponseEntity.created(new URI("/api/position-data/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /position-data} : Updates an existing positionData.
     *
     * @param positionDataDTO the positionDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated positionDataDTO,
     * or with status {@code 400 (Bad Request)} if the positionDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the positionDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/position-data")
    public Mono<ResponseEntity<PositionDataDTO>> updatePositionData(@RequestBody PositionDataDTO positionDataDTO) throws URISyntaxException {
        log.debug("REST request to update PositionData : {}", positionDataDTO);
        if (positionDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        return positionDataService.save(positionDataDTO)
            .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
            .map(result -> ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId()))
                .body(result)
            );
    }

    /**
     * {@code GET  /position-data} : get all the positionData.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of positionData in body.
     */
    @GetMapping("/position-data")
    public Mono<ResponseEntity<Flux<PositionDataDTO>>> getAllPositionData(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of PositionData");
        return positionDataService.countAll()
            .map(total -> new PageImpl<>(new ArrayList<>(), pageable, total))
            .map(page -> PaginationUtil.generatePaginationHttpHeaders(UriComponentsBuilder.fromHttpRequest(request), page))
            .map(headers -> ResponseEntity.ok().headers(headers).body(positionDataService.findAll(pageable)));
    }

    /**
     * {@code GET  /position-data/:id} : get the "id" positionData.
     *
     * @param id the id of the positionDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the positionDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/position-data/{id}")
    public Mono<ResponseEntity<PositionDataDTO>> getPositionData(@PathVariable String id) {
        log.debug("REST request to get PositionData : {}", id);
        Mono<PositionDataDTO> positionDataDTO = positionDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(positionDataDTO);
    }

    /**
     * {@code DELETE  /position-data/:id} : delete the "id" positionData.
     *
     * @param id the id of the positionDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/position-data/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deletePositionData(@PathVariable String id) {
        log.debug("REST request to delete PositionData : {}", id);
        return positionDataService.delete(id)            .map(result -> ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build()
        );
    }
}
