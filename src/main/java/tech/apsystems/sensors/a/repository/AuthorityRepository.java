package tech.apsystems.sensors.a.repository;

import tech.apsystems.sensors.a.domain.Authority;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

/**
 * Spring Data MongoDB repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends ReactiveMongoRepository<Authority, String> {
}
