package tech.apsystems.sensors.a.repository;

import tech.apsystems.sensors.a.domain.PositionData;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

/**
 * Spring Data MongoDB reactive repository for the PositionData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PositionDataRepository extends ReactiveMongoRepository<PositionData, String> {


    Flux<PositionData> findAllBy(Pageable pageable);

}
