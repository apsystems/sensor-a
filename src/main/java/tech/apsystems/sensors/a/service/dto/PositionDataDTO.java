package tech.apsystems.sensors.a.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link tech.apsystems.sensors.a.domain.PositionData} entity.
 */
public class PositionDataDTO implements Serializable {
    
    private String id;

    private Double lat;

    private Double lon;

    private Double altitude;

    private Instant date;

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PositionDataDTO)) {
            return false;
        }

        return id != null && id.equals(((PositionDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PositionDataDTO{" +
            "id=" + getId() +
            ", lat=" + getLat() +
            ", lon=" + getLon() +
            ", altitude=" + getAltitude() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
