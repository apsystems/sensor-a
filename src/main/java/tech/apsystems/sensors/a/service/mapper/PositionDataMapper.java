package tech.apsystems.sensors.a.service.mapper;


import tech.apsystems.sensors.a.domain.*;
import tech.apsystems.sensors.a.service.dto.PositionDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PositionData} and its DTO {@link PositionDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PositionDataMapper extends EntityMapper<PositionDataDTO, PositionData> {



    default PositionData fromId(String id) {
        if (id == null) {
            return null;
        }
        PositionData positionData = new PositionData();
        positionData.setId(id);
        return positionData;
    }
}
