package tech.apsystems.sensors.a.service;

import tech.apsystems.sensors.a.domain.PositionData;
import tech.apsystems.sensors.a.repository.PositionDataRepository;
import tech.apsystems.sensors.a.service.dto.PositionDataDTO;
import tech.apsystems.sensors.a.service.mapper.PositionDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 * Service Implementation for managing {@link PositionData}.
 */
@Service
public class PositionDataService {

    private final Logger log = LoggerFactory.getLogger(PositionDataService.class);

    private final PositionDataRepository positionDataRepository;

    private final PositionDataMapper positionDataMapper;

    public PositionDataService(PositionDataRepository positionDataRepository, PositionDataMapper positionDataMapper) {
        this.positionDataRepository = positionDataRepository;
        this.positionDataMapper = positionDataMapper;
    }

    /**
     * Save a positionData.
     *
     * @param positionDataDTO the entity to save.
     * @return the persisted entity.
     */
    public Mono<PositionDataDTO> save(PositionDataDTO positionDataDTO) {
        log.debug("Request to save PositionData : {}", positionDataDTO);
        return positionDataRepository.save(positionDataMapper.toEntity(positionDataDTO))
            .map(positionDataMapper::toDto)
;    }

    /**
     * Get all the positionData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Flux<PositionDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PositionData");
        return positionDataRepository.findAllBy(pageable)
            .map(positionDataMapper::toDto);
    }


    /**
    * Returns the number of positionData available.
    *
    */
    public Mono<Long> countAll() {
        return positionDataRepository.count();
    }

    /**
     * Get one positionData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Mono<PositionDataDTO> findOne(String id) {
        log.debug("Request to get PositionData : {}", id);
        return positionDataRepository.findById(id)
            .map(positionDataMapper::toDto);
    }

    /**
     * Delete the positionData by id.
     *
     * @param id the id of the entity.
     */
    public Mono<Void> delete(String id) {
        log.debug("Request to delete PositionData : {}", id);
        return positionDataRepository.deleteById(id);    }
}
